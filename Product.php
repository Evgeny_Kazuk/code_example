<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $table = 'product';

    protected $references_ids = [];

    /**
     * @return array
     */
    public function getReferencesIds()
    {
        if (empty($this->references_ids)){
            $this->references_ids = $this->references()->pluck('offer_id')->toArray();
        }

        return $this->references_ids;
    }

    public function getActiveReferences()
    {
        return $this->references()->where('active', 1)->pluck('offer_id')->toArray();
    }

    public function getReferences($onlyPrice = false)
    {
        $ids = $this->getReferencesIds();
        if (count($ids)>0){
            if ($onlyPrice){
                $references = Reference::whereIn('_id', $ids)->get(['price','_id']);
            }
            else {
                $references = Reference::whereIn('_id', $ids)->get();
            }

            return $references;
        }

        return [];
    }

    public function getCategoryName()
    {
        $category = $this->category()->first(['title']);

        if ($category){
            return $category->title;
        }

        return '';
    }
    public function category()
    {
        return $this->hasOne('App\Category', 'id', 'category_id');
    }

    public function references()
    {
        return self::hasMany('App\ProductReference','product_id', 'id');
    }

}