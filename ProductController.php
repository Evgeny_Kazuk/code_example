<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Product;
use App\ProductReference;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function index(Request $request)
    {
        $sortBy = 'title';
        $dir = 'asc';
        switch ($request->input('direction')) {
            case "asc":
            case "desc":
                $dir = $request->input('direction');
                session(['direction' => $dir]);
                break;
            default:
                $dir = 'asc';
                session(['direction' => $dir]);
        }
        switch ($request->input('sortBy')) {
            case "price":
                session(['sortBy' => 'price_min']);
                $sortBy = 'price_min';
                break;
            case "title":
                session(['sortBy' => 'title']);
                $sortBy = 'title';
                break;
            default:
                if ($request->session()->get('sortBy')) {
                    $sortBy = $request->session()->get('sortBy');
                }
        }

        $products = Product::select(['id', 'title', 'description', 'price_min', 'price_max', 'vendor', 'image', 'category', 'active']);
        $filter = [
            'query' => $request->input('query', null),
            'price_min' => $request->input('price_min', null),
            'price_max' => $request->input('price_max', null),
            'brands' => $request->input('brands', []),
        ];

        $products->orderBy($sortBy, $dir);
        if (!empty($filter['query'])) {
            $products->where('title', 'like', "%{$filter['query']}%");
        }
        if (!empty($filter['price_min'])) {
            $products->where(function ($query) use ($filter) {
                $query->where('price_min', '>=', (int)$filter['price_min']);
                $query->orWhere('price_max', '>=', (int)$filter['price_min']);
            });
        }
        if (!empty($filter['price_max']) && $filter['price_max'] > 0) {
            $products->where(function ($query) use ($filter) {
                $query->where('price_min', '<=', (int)$filter['price_max']);
                $query->orWhere('price_max', '<=', (int)$filter['price_max']);
            });
        }
        if (!empty($filter['brands'])) {
            $products->where(function ($query) use ($filter) {
                $i = 0;
                foreach ($filter['brands'] as $name) {
                    $i++;
                    if ($i == 1) {
                        $query->where('vendor', $name);
                    } else {
                        $query->orWhere('vendor', $name);
                    }
                }
            });
        }

        $products = $products->paginate(10);
        $brands = Product::select('vendor')->whereNotNull('vendor')->where('vendor', '!=', '')->groupBy('vendor')->pluck('vendor');

        return view('admin.product.products', compact('products', 'filter', 'brands', 'sortBy', 'dir'));
    }

    public function updateArchive(Request $request)
    {
        $product = Product::find($request->input('id', 0));
        if ($product) {
            if ($product->active == 1) {
                $product->active = 0;
            } else {
                $product->active = 1;
            }
            $product->save();

            return response()->json(['status' => $product->active]);
        }

        return response()->json(['failure' => true], 404);
    }

    public function details($id)
    {
        $product = Product::find($id);
        if ($product) {
            $references = $product->getReferences();

            return view('admin.product.product_detail', compact(['product', 'references']));
        }

        return abort(404);
    }

    public function edit($id, Request $request)
    {
        $product = Product::find($id);
        if ($product instanceof Product) {
            $references = $product->getReferences();
            if (($changeDescriptionTo = $request->input('changeDescription'))!=null){
                foreach ($references as $reference){
                    if ($reference->_id == $changeDescriptionTo){
                        $product->title = $reference->title;
                        $product->offer_id = $reference->_id;
                        $product->description = $reference->description;
                        $product->vendor = $reference->vendor;
                        $product->vendor_code = $reference->vendor_code;
                        $product->category = $reference->category;
                        $product->save();

                        break;
                    }
                }
                return redirect()->route('admin_product_edit', ['id' => $id]);
            }
            $activeReferences = $product->getActiveReferences();
            $images = [];
            foreach ($references as $reference){
                if (isset($reference->images) && is_array($reference->images)){
                    foreach ($reference->images as $image){
                        $images[] = $image;
                    }
                }
            }

            if ($request->getMethod() == 'POST') {

                $productInfo = $request->input('product');

                $only = ['title', 'category', 'description', 'vendor', 'image'];
                foreach ($productInfo as $name => $value) {
                    if (in_array($name, $only)) {
                        $product->{$name} = $value;
                    }
                }

                if (isset($productInfo['offer_id'])) {
                    if (in_array($productInfo['offer_id'], $product->getReferencesIds())) {
                        $product->offer_id = $productInfo['offer_id'];
                    }
                }

                $referencesInfo = $request->input('references');
                $prices = [];
                $ids = [];
                foreach ($product->references()->get() as $reference) {
                    if ($reference instanceof ProductReference) {
                        $active = 0;
                        if (isset($referencesInfo[$reference->offer_id])) {
                            $active = 1;
                            $ids[] = $reference->offer_id;
                        }
                        \DB::table('product_reference')
                            ->where(['product_id' => $reference->product_id, 'offer_id' => $reference->offer_id])
                            ->update(['active' => $active]);
                    }
                }
                foreach ($references as $reference) {
                    if (in_array($reference->_id, $ids)) {
                        $prices[] = $reference->price;
                    }
                }

                $prices = array_filter($prices);
                if (empty($prices)) {
                    $price_min = 0;
                    $price_max = 0;
                } else {
                    $price_min = min($prices);
                    $price_max = max($prices);
                    if (!$price_min) {
                        $price_min = 0;
                    }
                    if (!$price_max) {
                        $price_max = 0;
                    }
                }

                $product->price_min = $price_min;
                $product->price_max = $price_max;
                $product->save();
                $activeReferences = $ids;
            }

            return view('admin.product.product_detail_edit', compact(['product', 'references', 'activeReferences', 'images']));
        }

        return abort(404);
    }
}